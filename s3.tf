resource "aws_s3_bucket" "chuckitbucket" {
  bucket = "chuckitbucket-epj"
  acl    = "public-read"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
}