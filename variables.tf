variable "region" {
    type = string
    description = "AWS Region"
    default = "us-east-1"
}

variable "access_key" {
    type = string
    description = "Access key ID"
}

variable "secret_key" {
    type = string
    description = "Secret key"
}