resource "aws_api_gateway_rest_api" "chuckitbucket" {
  name          = "chuckitbucket-api"
}

resource "aws_api_gateway_resource" "chuckitbucket_resource" {
  rest_api_id   = aws_api_gateway_rest_api.chuckitbucket.id
  parent_id     = aws_api_gateway_rest_api.chuckitbucket.root_resource_id
  path_part     = "chuckitbucket"
}

resource "aws_api_gateway_resource" "chuckitbucket_list_resource" {
  rest_api_id   = aws_api_gateway_rest_api.chuckitbucket.id
  parent_id     = aws_api_gateway_rest_api.chuckitbucket.root_resource_id
  path_part     = "list"
}

resource "aws_api_gateway_method" "get_chuckitbucket" {
  rest_api_id   = aws_api_gateway_rest_api.chuckitbucket.id
  resource_id   = aws_api_gateway_resource.chuckitbucket_resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "get_chuckitbucket_integration" {
   rest_api_id = aws_api_gateway_rest_api.chuckitbucket.id
   resource_id = aws_api_gateway_method.get_chuckitbucket.resource_id
   http_method = aws_api_gateway_method.get_chuckitbucket.http_method

   integration_http_method = "GET"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.readFromS3.invoke_arn
}

resource "aws_api_gateway_method" "post_chuckitbucket" {
  rest_api_id   = aws_api_gateway_rest_api.chuckitbucket.id
  resource_id   = aws_api_gateway_resource.chuckitbucket_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "post_chuckitbucket_integration" {
   rest_api_id = aws_api_gateway_rest_api.chuckitbucket.id
   resource_id = aws_api_gateway_method.post_chuckitbucket.resource_id
   http_method = aws_api_gateway_method.post_chuckitbucket.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.writeToS3.invoke_arn
}

resource "aws_api_gateway_method" "list_chuckitbucket" {
  rest_api_id   = aws_api_gateway_rest_api.chuckitbucket.id
  resource_id   = aws_api_gateway_resource.chuckitbucket_list_resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "list_chuckitbucket_integration" {
   rest_api_id = aws_api_gateway_rest_api.chuckitbucket.id
   resource_id = aws_api_gateway_method.list_chuckitbucket.resource_id
   http_method = aws_api_gateway_method.list_chuckitbucket.http_method

   integration_http_method = "GET"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.listObjectsS3.invoke_arn
}

resource "aws_api_gateway_deployment" "api_deployment" {
   depends_on = [
     aws_api_gateway_integration.get_chuckitbucket_integration,
     aws_api_gateway_integration.post_chuckitbucket_integration,
     aws_api_gateway_integration.list_chuckitbucket_integration,
   ]

   rest_api_id = aws_api_gateway_rest_api.chuckitbucket.id
   stage_name  = "chuckitbucket"
}


resource "aws_lambda_permission" "chuckitbucket_apigw_readFromS3" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.readFromS3.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.chuckitbucket.execution_arn}/*/*"
}

resource "aws_lambda_permission" "chuckitbucket_apigw_writeToS3" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.writeToS3.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.chuckitbucket.execution_arn}/*/*"
}

resource "aws_lambda_permission" "chuckitbucket_apigw_listObjectsS3" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.listObjectsS3.function_name
   principal     = "apigateway.amazonaws.com"
   source_arn = "${aws_api_gateway_rest_api.chuckitbucket.execution_arn}/*/*"
}

output "chuckitbucket_api_url" {
  value = aws_api_gateway_rest_api.chuckitbucket.execution_arn
}