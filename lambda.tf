### readFromS3

data "archive_file" "zip_readFromS3" {
    type        = "zip"
    source_dir  = "${path.module}/lambda/readFromS3/"
    output_path = "${path.module}/lambda/readFromS3/readFromS3.zip"
}

resource "aws_lambda_function" "readFromS3" {
  filename      = "${path.module}/lambda/readFromS3/readFromS3.zip" 
  function_name = "readFromS3"
  handler       = "index.handler"
  runtime       = "nodejs18.x"
  role          = aws_iam_role.iam_for_lambda.arn
  depends_on    = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

output "readFromS3_arn" {
  value = aws_lambda_function.readFromS3.arn
}


### writeToS3

data "archive_file" "zip_writeToS3" {
    type        = "zip"
    source_dir  = "${path.module}/lambda/writeToS3/"
    output_path = "${path.module}/lambda/writeToS3/writeToS3.zip"
}

resource "aws_lambda_function" "writeToS3" {
  filename      = "${path.module}/lambda/writeToS3/writeToS3.zip" 
  function_name = "writeToS3"
  handler       = "index.handler"
  runtime       = "nodejs18.x"
  role          = aws_iam_role.iam_for_lambda.arn
  depends_on    = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

output "writeToS3_arn" {
  value = aws_lambda_function.readFromS3.arn
}


### listObjectsS3 

data "archive_file" "zip_listObjectsS3" {
    type        = "zip"
    source_dir  = "${path.module}/lambda/listObjectsS3/"
    output_path = "${path.module}/lambda/listObjectsS3/listObjectsS3.zip"
}

resource "aws_lambda_function" "listObjectsS3" {
  filename      = "${path.module}/lambda/listObjectsS3/listObjectsS3.zip"
  function_name = "listObjectsS3"
  handler       = "index.handler"
  runtime       = "nodejs18.x"
  role          = aws_iam_role.iam_for_lambda.arn
  depends_on    = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

output "listObjectsS3_arn" {
  value = aws_lambda_function.listObjectsS3.arn
}