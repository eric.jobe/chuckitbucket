const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");

const REGION = "us-east-1";
const s3Client = new S3Client({region: REGION});

exports.handler = async (event) => {
    const key = event.key;
    const params = {
        Bucket: "chuckitbucket-epj",
        Key: key
    };

    try {
        const s3Response = await s3Client.send(new GetObjectCommand(params));
        const body = s3Response.Body.transformToString();
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'File downloaded successfully',
                data: body
            }),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: error.message
            })
        };
    }
}