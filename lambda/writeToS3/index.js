const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");

const REGION = "us-east-1";
const s3Client = new S3Client({region: REGION});

exports.handler = async (event) => {
    const params = {
        Bucket: "chuckitbucket-epj",
        Key: event.id+"-"+event.file,
        Body: event.body,
    };

    try {
        const s3Response = await s3Client.send(new PutObjectCommand(params));
        console.log(s3Response);
        return {
            'statusCode': 200,
            'headers': { 
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST',
                'Content-Type': 'application/json' 
            },
            'body': JSON.stringify({
                "id": event.id,
                "message": "File uploaded successfully",
                "data":s3Response
            })
        };
    } catch (error) {
        return {
            'statusCode': 500,
            'headers': { 'Content-Type': 'application/json' },
            'body': JSON.stringify({
                "error": error
            })
        };
    }
}