const { S3Client, ListObjectsCommand } = require("@aws-sdk/client-s3");

const REGION = "us-east-1";
const s3Client = new S3Client({region: REGION});
const params = { Bucket: "chuckitbucket-epj" };

exports.handler = async (event) => {
    try {
        const data = await s3Client.send(new ListObjectsCommand(params));
        console.log("Success", data);
        const objects = data.Contents.map( object => object.Key )
        return {
            statusCode: 200,
            'headers': {
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            body: JSON.stringify({objects}),
        };  
    } catch (err) {
        console.log("Error", err);
        return {
            statusCode: 500,
            body: JSON.stringify({err}),
        };  
    }
}